lighttpd config (cgi):
-----------------

	index-file.names = ("bm2.py")
	cgi.assign = ("bm2.py" => "/usr/bin/python")
	url.rewrite-once = (
	"^/favicon.ico$" => "/files/favicon.ico",
	"^/files/(.*)$" => "/files/$1",
	"^/screenshots/(.*)$" => "/screenshots/$1",
	"^/c/(.*)$" => "/c/$1",
	"^/(.*)$" => "/bm2.py/$1",
	)
	


nginx config:
-------------

	# static directories (/files, /screenshots, /download, etc)
	# repeat for each
	
	location /files {
		root /var/www/bm2;
		autoindex on;
	}


	# wsgi 
	location / {
	                root   /var/www/bm2;
	                proxy_set_header Host $http_host;
	                proxy_set_header X-Forwarded-Host $http_host;
	                proxy_set_header X-Real-IP $remote_addr;
	                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	                proxy_pass http://localhost:9009;
	
	        }

Apache 2 config (not tested):
---------------

1. 
	apt-get install libapache2-mod-wsgi

2. 

	LoadModule wsgi_module modules/mod_wsgi.so

	WSGIScriptAlias / /var/www/bm2/bm2.py/

	Alias /files /var/www/bm2/files/
	AddType text/html .py

	<Directory /var/www/bm2/>
		Order deny,allow
		Allow from all
	</Directory>

