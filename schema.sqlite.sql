CREATE TABLE news (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT,
    content TEXT,
    posted_on DATETIME
);
create table converter (
 smiley_id integer primary key autoincrement,
 smiley_content text
 );
CREATE TABLE downloads (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    version TEXT,
    content BLOB,
    downloaded int not null default 0,
    uploaded_on DATETIME
);
CREATE TABLE resources (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    typeid INTEGER,
    name TEXT,
    author TEXT,
    content BLOB,
    compressed BOOLEAN,
    uploaded_on DATETIME,
    download_count INTEGER
);
CREATE TABLE restypes (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT
);

insert into restypes(name) values("Smiles");
insert into restypes(name) values("Skin");
insert into restypes(name) values("Moods");
insert into restypes(name) values("Language pack");
insert into restypes(name) values("Sound");
insert into restypes(name) values("Splash");
insert into restypes(name) values("Background");
insert into restypes(name) values("Color scheme");
insert into restypes(name) values("Application icon");
