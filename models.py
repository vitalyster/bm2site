#!/usr/bin/env python

import web, datetime
import os.path

import config

db = web.database(dbn='sqlite', db=os.path.join(config.db_root, 'bm2.s3db'))

class news:
	def get_news(self):	
		"""
				
		"""
		return db.select('news', order='id DESC')
	def get_entry(self, id):
		try:
			return db.select('news', where='id=$id', vars=locals())[0]
		except IndexError:
			return None
	def add_entry(self, title, text):
		db.insert('news', title=title, content=text, posted_on=datetime.datetime.utcnow())
	def remove_entry(self, id):
		"""

		"""
		return db.delete('news', where="id=$id", vars=locals())

class converter:
	def get(self, _id):
		return db.select('converter', where='smiley_id=$_id', vars=locals())[0]
	def put(self, data):
		return db.insert('converter', smiley_content=buffer(data))

class downloader:
	def get_jars(self):
		return db.select('downloads', order='id DESC')
	def add_jar(self, version, content):
		db.insert('downloads', version=version, content=buffer(content), uploaded_on=datetime.datetime.utcnow())
	def remove_jar(self, id):
		return db.delete('downloads', where="id=$id", vars=locals())
	def increment_download(self, id):
		v = db.select('downloads', where='id=$id', vars=locals())[0]
		count = v.downloaded + 1
		db.update('downloads', where='id=$id', vars=locals(), downloaded = count)

class resources:
	def get_entries(self):
		return db.query('select r.id as id, r.name as rname, t.name as tname, r.compressed as compressed from resources as r inner join restypes as t where t.id=r.typeid order by r.id DESC')		
	def get_smiles(self):
		return db.query('select id, name, content from resources where typeid=1')
	def get_types(self):
		return db.select("restypes")
	def add_entry(self, typeid, name, author, content, compressed):
		db.insert("resources", typeid=typeid, name=name, author=author, content=buffer(content), compressed=compressed, uploaded_on=datetime.datetime.utcnow())
	def remove_entry(self, id):
		return db.delete('resources', where="id=$id", vars=locals())
