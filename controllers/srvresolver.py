import re

import web
import dns.resolver

def isValidHostname(hostname):
	if len(hostname) > 255:
		return False
	if hostname[-1:] == ".":
		hostname = hostname[:-1] # strip exactly one dot from the right, if present
	allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
	return hostname, all(allowed.match(x) for x in hostname.split("."))

bad_hosts = ('nimbuzz', 'www.nimbuzz.com', '@nimbuzz.com', 'nimbuz.com', 'nimauzz.com', 'm.nimbuzz.com')

class srvresolver:
	def GET(self, host, name):
		(hostname, valid) = isValidHostname(host)
		if valid and name in ("BombusMod", "gluxibot") and host not in bad_hosts:
			web.header('Content-Type', 'text/html')
			try:
				answers = dns.resolver.query("_xmpp-client._tcp." + hostname, "SRV")
			except:
				return "%s have no SRV records" % hostname
			if len(answers) > 0:
				result = answers[0]		
				return "\nhost\t%s\nport\t%s\nttl\t%d\n" % (str(result.target)[:-1], str(result.port), 60)
			return "%s have no SRV records" % hostname
		return web.notfound()