import web
from web.contrib.template import render_jinja
from markdown import markdown

import cgi
import os.path
import re
import sys
from StringIO import StringIO
import zipfile

import config
import models


render = render_jinja(os.path.join(config.site_root, 'views/admin'))

class admin:
	def GET(self):

		news = models.news()
		versions = models.downloader()		
		web.header('Content-type', 'text/html')

		return render.admin(posts=news.get_news(), versions = versions.get_jars())
	def POST(self):
		web.header('Content-type', 'text/html')
		input = web.input()
		new = models.news()
		new.add_entry(input.caption, markdown(input.news))
		return render.admin_success(success='admin')

class admin_delete:
	def GET(self, id):
		news = models.news()
		news.remove_entry(int(id))
		web.header('Content-type', 'text/html')

		return render.admin_success(success='admin')
class admin_vdelete:
	def GET(self, id):
		downloads = models.downloader()
		downloads.remove_jar(int(id))
		web.header('Content-type', 'text/html')

		return render.admin_success(success='admin')
class admin_rdelete:
	def GET(self, id):
		res = models.resources()
		res.remove_entry(int(id))
		web.header('Content-type', 'text/html')

		return render.admin_success(success='admin')

class admin_add:
	def GET(self):
		web.header('Content-type', 'text/html')

		return render.admin_add()

cgi.maxlen = 1024 * 1024 # 1024kb

class admin_upload:
	def GET(self):
		web.header('Content-type', 'text/html')

		return render.admin_upload()
	def POST(self):
		try:
			x = web.input(myfile={})
		except ValueError:
			raise web.seeother("/admin/upload")
		content = x['myfile'].file.read()
		fsize = len(content)
		try:
			zfp = zipfile.ZipFile(StringIO(buffer(content)), "r")
		except zipfile.BadZipfile:
			return render.admin_not_a_jar()
		pattern = re.compile('0\.8\.1440\.\d{4,}')
		version = ""
		for name in zfp.namelist():
			next = zfp.read(name)
			result = pattern.search(next)
			if result:
				version = result.group()
				break
		web.header('Content-type', 'text/html')
	
		if version == "": return render.admin_not_a_jar()
		downloader = models.downloader()
		downloader.add_jar(version, content)
		return render.admin_upload_success(size=fsize, zip = version)

class admin_resources:
	def GET(self):
		res = models.resources()
		all = res.get_entries()
		web.header('Content-type', 'text/html')

		return render.admin_resources(resources=all)
class admin_resources_add:
	def GET(self):
		types = models.resources()        
		web.header('Content-type', 'text/html')

		return render.admin_resources_add(types=types.get_types())
	def POST(self):
		try:
			x = web.input(myfile={})            
		except ValueError:
			raise web.seeother("/admin/resources")
		res = models.resources()
		res.add_entry(x.type, x.name, x.author, x['myfile'].file.read(), hasattr(x, 'compressed'))
		web.header('Content-type', 'text/html')

		return render.admin_resources(resources=res.get_entries())
