import web
from web.contrib.template import render_jinja

import os.path

import config
import models

render = render_jinja(os.path.join(config.site_root, 'views/admin'))


class constructor:
	def GET(self):
		res = models.resources()
		res_smiles = res.get_smiles()
		web.header('Content-type', 'text/html')
		return render.c(smiles=res_smiles)
	def POST(self):
		web.header('Content-type', 'text/html')
		return render.c_success()	