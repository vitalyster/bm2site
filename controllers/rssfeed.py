#!/usr/bin/python
# -*- coding: utf-8 -*-
import web
from dateutil.parser import parse
import PyRSS2Gen as RSS2
import os.path

import config
import models

class rssfeed:
	def GET(self):
		rss = RSS2.RSS2(
		title = "BombusMod News",
		link = config.site_url,
		description = "Обновления BombusMod",
		language = "ru",
		ttl = 30,
		)
		model = models.news()
		news = model.get_news()
		lastid = 0
		for item in news:
			newsurl = "/news/" + str(item.id)
			if item.id > lastid:
				rss.lastBuildDate = web.httpdate(parse(item.posted_on)) # web.py workaround
				rss.pubDate = rss.lastBuildDate
				lastid = item.id
			rss.items.append (
			  RSS2.RSSItem (
				title = item.title,
				link = newsurl,
				description = item.content,
				guid = RSS2.Guid(newsurl),
				pubDate = web.httpdate(parse(item.posted_on))
			  )
			)
		web.header('Content-Type', 'application/rss+xml')		
		return rss.to_xml(encoding="utf-8")