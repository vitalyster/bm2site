import web
from web.contrib.template import render_jinja

import os.path

import config
import models


render = render_jinja(os.path.join(config.site_root, 'views'))



class flatpages:
	def GET(self, name):
		if name == '': name = 'index'			
		d = models.downloader()
		jars = list(d.get_jars())
		version = '0.000.000'
		if jars:
			version = jars[0].version
		web.header('Content-Type', 'text/html')		
		try:
			return getattr(render, name)(android=("Android" in web.ctx.env["HTTP_USER_AGENT"]), mobile = (len(set(["HTTP_X_WAP_PROFILE", "HTTP_X_OPERAMINI_FEATURES"]) & set(web.ctx.env.keys()))>0), version=version)
		except:
			raise web.notfound()		

def notfound():
	web.header('Content-type', 'text/html')

	return web.notfound(render.notfound())
def internalerror():
	web.header('Content-type', 'text/html')

	return web.internalerror(render.internalerror())

class version:
	def GET(self):
		d = models.downloader()
		jars = list(d.get_jars())
		version = '0.000.000'
		if jars:
			version = jars[0].version
		web.header('Content-Type', 'text/html')
		return version + "\n" + config.site_url+"download/BombusMod.jar" + "\n"  + config.site_url+"download/BombusMod.jad"