import web
from web.contrib.template import render_jinja

import os.path

import config
import models

render = render_jinja(os.path.join(config.site_root, 'views'))

class news:
	def GET(self):
		model = models.news()
		news = model.get_news()
		web.header('Content-type', 'text/html')
		return render.news(mobile = (len(set(["HTTP_X_WAP_PROFILE", "HTTP_X_OPERAMINI_FEATURES"]) & set(web.ctx.env.keys()))>0), posts=news)

class newsviewer:
	def GET(self, name):
		model = models.news()
		newsfull = model.get_entry(id=name)
		web.header('Content-type', 'text/html')
		return render.fullnews(mobile = (len(set(["HTTP_X_WAP_PROFILE", "HTTP_X_OPERAMINI_FEATURES"]) & set(web.ctx.env.keys()))>0), post=newsfull)
