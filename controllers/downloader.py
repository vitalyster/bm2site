import web
import models
import web.http
from dateutil.parser import parse


JAD = "Background: True\nBombus-Version: %s\nFlipInsensitive: True\nMIDlet-1: BombusMod,/_icon.png,midlet.BombusMod\nMIDlet-Description: MIDP Jabber Client\nMIDlet-Icon: /_icon.png\nMIDlet-Jar-Size: %d\nMIDlet-Jar-URL: BombusMod.jar\nMIDlet-Name: BombusMod\nMIDlet-Vendor: Evgs, ad\nMIDlet-Version: 0.8\nMicroEdition-Configuration: CLDC-1.1\nMicroEdition-Profile: MIDP-2.0\nNokia-MIDlet-No-Exit: true\nNokia-MIDlet-On-Screen-Keypad: no\nNokia-MIDlet-S60-Selection-Key-Compatibility: true"

class download_jad:
	def GET(self):
		last = models.downloader().get_jars()[0]		
		web.header('Content-Type', 'text/vnd.sun.j2me.app-descriptor')
		web.http.lastmodified(parse(last.uploaded_on))	
		return JAD % (last.version, len (last.content))

class download_jar:
	def GET(self):
		last = models.downloader().get_jars()[0]
		lenbytes = len(last.content)
		httprange = web.ctx.env.get('HTTP_RANGE')
		if httprange is not None:
			_, r = httprange.split("=")
			part_start, part_end = r.split("-")
			start = int(part_start)
			if not part_end:
				end = lenbytes - 1
			else:
				end = int(part_end)
			chunksize = (end - start) + 1
			web.ctx.status = "206 Partial Content"
			web.header("Content-Range", "bytes %d-%d/%d" % (start, end, lenbytes))
			web.header("Accept-Ranges", "bytes")
			web.header("Content-Length", chunksize)
			web.header('Content-Type', 'application/x-java-archive')
			web.http.lastmodified(parse(last.uploaded_on))
			return last.content[start:end+1]
		models.downloader().increment_download(last.id)		
		web.header('Content-Type', 'application/x-java-archive')
		web.http.lastmodified(parse(last.uploaded_on))
		web.header("Content-Length", lenbytes)
		return last.content
	