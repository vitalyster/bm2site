import web
from web.contrib.template import render_jinja

import re
from zipfile import ZipFile
from StringIO import StringIO
import os.path
import cgi

import config
from models import converter


render = render_jinja(os.path.join(config.site_root, 'views'))
cgi.maxlen = 20 * 1024 # 20kb
class uploader:
	def GET(self):
		web.header('Content-type', 'text/html')

		return render.converter(mobile = (len(set(["HTTP_X_WAP_PROFILE", "HTTP_X_OPERAMINI_FEATURES"]) & set(web.ctx.env.keys()))>0))

	def POST(self):
		try:
			x = web.input(myfile={})
		except ValueError:
			raise web.seeother("/utils/converter")
		content = x['myfile'].file.readlines()
		web.header('Content-Type', 'text/html')
		lines = []
		for line in content[1:]:
			newline = re.split('\s+', line, 2)[-1]
			lines.append(newline.replace(", ", "\t"))
		result = "".join(lines)
		newsmile = converter()
		newid = newsmile.put(result)
		return render.converter_success(mobile = (len(set(["HTTP_X_WAP_PROFILE", "HTTP_X_OPERAMINI_FEATURES"]) & set(web.ctx.env.keys()))>0), id=newid)

class downloader:
	global result
	def GET(self, _id, format):
#		if not isinstance(_id, int):
#			raise web.seeother("/utils/converter")
		if format == "zip":
			mm = StringIO()
			zip = ZipFile(mm, 'w')
			getsmile = converter()
			res = getsmile.get(_id)
			zip.writestr('smiles.txt', res.smiley_content)
			zip.close()
			web.header('Content-Type', 'multipart/x-zip')		
			return mm.getvalue()		
		if format == "txt":
			getsmile = converter()
			res = getsmile.get(_id)
			web.header('Content-Type', 'text/plain')
			return res.smiley_content
		web.header('Content-type', 'text/html')

		return web.notfound()		
