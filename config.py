#!/usr/bin/env python

debug = False
site_url = 'http://bombusmod.net.ru/'
site_root = '/var/www-bm'
db_root = '/var/spool/bm2'

try:
	from devel_config import *
except ImportError:
	pass