#!/usr/bin/env python

import web
import os.path

import config

from controllers.converter import uploader, downloader
from controllers.rssfeed import rssfeed
from controllers.news import news, newsviewer
from controllers.admin import admin, admin_delete, admin_add, admin_upload, admin_vdelete, admin_resources, admin_resources_add, admin_rdelete
from controllers.flatpages import flatpages, notfound, internalerror, version
from controllers.srvresolver import srvresolver
from controllers.downloader import download_jad, download_jar
from controllers.constructor import constructor

urls = (
	"/bombusmod.xml", "rssfeed",
	"/download/BombusMod.jad", "download_jad",
	"/download/BombusMod.jar", "download_jar",
	"/utils/converter", 'uploader',
	"/utils/converter/(\d+)/smiles.(zip)", 'downloader',
	"/utils/converter/(\d+)/smiles.(txt)", 'downloader',
	"/admin", "admin",
	"/admin/c", "constructor",
	"/admin/add", "admin_add",
	"/admin/delete/(\d+)", 'admin_delete',
	"/admin/upload", "admin_upload",
	"/admin/vdelete/(\d+)", 'admin_vdelete',
	"/admin/resources", "admin_resources",
	"/admin/resources/add", "admin_resources_add",
	"/admin/resources/delete/(\d+)", 'admin_rdelete',
	"/news/(\d+)", 'newsviewer',
	"/news", 'news',
	"/srv/(.*)/(.*)", "srvresolver",
	"/ver", "version",
	"/(.*)", 'flatpages',
	)

web.config.debug = config.debug
app = web.application(urls, globals(), True)
if not config.debug:
	app.notfound = notfound
	app.internalerror = internalerror
if __name__ == "__main__": app.run()
